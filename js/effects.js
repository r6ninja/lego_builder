/**
 * Setup the base prototype scene along with the first scene
 */
import * as THREE from './3rdParty/three.module.js'
export const effects = (function() {
    return {
        Main: class {

            constructor() {
                this._Initialize();
            }

            _Initialize() {
                let that = this;

                this.start = Date.now();

                // Load sounds
                this.setupSounds();

                // Initiate the event listeners
                this.events();

                // Start the scene animation
                this.animateMe = function() {
                    that.animate();
                };

                LOADED++;
            }

            // Calls the applicable functions to animate the scene
            animate() {
                requestAnimationFrame(this.animateMe);
                this.update();
            }

            // Updates the variables for every frame iteration
            update() {
            }

            events() {
                let that = this;

                /*window.addEventListener("effectsExplode", function(event) {
                    that.explosion(event.detail);
                }, false);*/
            }

            setupSounds() {
                let that = this;
                /*this.explodeSound = new THREE.Audio( listener );
                audioLoader.load( 'sounds/effects/explosion.mp3', function( buffer ) {
                    that.explodeSound.setBuffer( buffer );
                    that.explodeSound.setLoop( false );
                    that.explodeSound.setVolume( 0.5 );
                });*/
            }
        }
    }
})();