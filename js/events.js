export const events = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this._Initialize();
            }

            _Initialize() {
                this.events();

                LOADED++;
            }

            onDocumentKeyDown(event) {
                if (event.keyCode === 219) {
                    let newEvent = new Event('playerControlsOn');
                    window.dispatchEvent(newEvent);

                    let newEvent2 = new Event('shipControlsOff');
                    window.dispatchEvent(newEvent2);
                }

                if(event.keyCode === 221) {
                    let newEvent = new Event('shipControlsOn');
                    window.dispatchEvent(newEvent);

                    let newEvent2 = new Event('playerControlsOff');
                    window.dispatchEvent(newEvent2);
                }

                // Pause
                if (event.keyCode === 80) {
                    console.log('pause');
                    if (PawsOn < 1) PawsOn = 1;
                    else PawsOn = 0;
                }
            }

            // Initialise object specific events
            events() {
                var that = this;
                document.addEventListener("keydown", function(event) { that.onDocumentKeyDown(event); }, false);
            }
        }
    };
})();