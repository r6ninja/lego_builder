/**
 * Player character
 */
import * as THREE from './3rdParty/three.module.js'

import {GLTFLoader} from './3rdParty/loaders/GLTFLoader.js'
import {DRACOLoader} from './3rdParty/loaders/DRACOLoader.js'
const TextureLoader = new THREE.TextureLoader();

const gltfLoaderDraco = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath( 'js/3rdParty/loaders/draco/gltf/' );
dracoLoader.setDecoderConfig({type:'js'});
gltfLoaderDraco.setDRACOLoader( dracoLoader );

export const shadowScreen = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                var that = this;

                this.LoadModel();
                this.events();
            }

            events() {
                let that = this;
                window.addEventListener('changeT', function (e) {
                    that.model.traverse( function ( object ) {
                        if(object.name === 'Plane001_green_0') {
                            object.material.map = TextureLoader.load(templates[e.detail.id].template, function ( texture ) {
                                texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                                texture.repeat.set(-0.515, 0.5);
                                //texture.repeat.x = -1;
                                texture.offset.set(1.247, 0.243);
                                //texture.rotation.set(0, -Math.PI/2, 0);
                                texture.rotation = THREE.Math.degToRad(-90);
                            })
                        }
                    });
                });
            }

            LoadModel() {
                let that        = this;
                let path        = "./models/";
                let modelFile   = "screen4.glb";
                let file        = path + modelFile;

                gltfLoaderDraco.load(file, function (gltf) {
                    that.model = gltf.scene;
                    that.model.traverse( function ( object ) {
                        if ( object.isMesh) {
                            //object.layers.enable( BLOOM_SCENE );
                            object.receiveShadow = true;
                            object.castShadow = true;
                            object.material.side = THREE.FrontSide;
                            if(object.name === 'Plane001_green_0') {
                                object.material = new THREE.MeshPhongMaterial({
                                    transparent:false,
                                    //map: TextureLoader.load('images/templates/template4.png', function ( texture ) {
                                    map: TextureLoader.load(templates[1].template, function ( texture ) {
                                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                                        texture.repeat.set(-0.515, 0.5);
                                        //texture.repeat.x = -1;
                                        texture.offset.set(1.247, 0.243);
                                        //texture.rotation.set(0, -Math.PI/2, 0);
                                        texture.rotation = THREE.Math.degToRad(-90);
                                    }),
                                });
                            }
                            object.material.onBeforeCompile = ModifyShader_;
                        }
                    });

                    that.model.scale.set(1.5, 1.5, 1.5);
                    that.model.position.set(-290, 5, -20);
                    that.model.name = 'shadowScreen';
                    //that.model.rotation.y = THREE.Math.degToRad(-180);
                    that.currentScene.scene.add(that.model);

                    that.loaded = true;

                    LOADED++;
                });
            }
        }
    }
})();