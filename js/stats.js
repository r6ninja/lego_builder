/**
 * Setup the base prototype scene along with the first scene
 */
import Stats from './3rdParty/stats.module.js';
export const stats = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this._Initialize();
            }

            _Initialize() {
                let that = this;

                this.stats = new Stats();
                this.stats.domElement.style.position = 'absolute';
                this.stats.domElement.className = 'stats';
                document.body.appendChild(this.stats.domElement);

                // Start the scene animation
                this.animateMe = function() {
                    that.animate();
                };

                LOADED++;
            }

            // Calls the applicable functions to animate the scene
            animate() {
                requestAnimationFrame(this.animateMe);
                this.update();
            }

            // Updates the variables for every frame iteration
            update() {
                this.stats.update();
            }
        }
    }
})();