/* General -----------------------------------------------------------------*/
var WINDOW_HEIGHT = window.innerHeight;
var WINDOW_WIDTH = window.innerWidth;
var WINDOW_HEIGHT_MOBILE = WINDOW_HEIGHT;
var WINDOW_WIDTH_MOBILE = WINDOW_WIDTH;
var WINDOW_PIXEL_RATIO = window.devicePixelRatio;
var LOADED          = 0;
var USE_PHYSICS     = false;
var BETTER_FOG      = false;
var BETTER_FOG2     = true;
var FLUID_CAMERA    = false;
var MOBILE          = false;
var MUSIC           = false;
var WINDOW_ACTIVE   = true;
var SIMULATE        = false;
var captureTemplate = false;

// Quality controls
var FOG             = QUALITY;
var POST_PROCESSING = QUALITY;

if(captureTemplate)
    POST_PROCESSING = false;

/**
 * Post Processing
 */
var PP_GUI          = false;
var PP_BLOOM        = false;
var PP_BLOOM_SELECT = false;
var PP_SCAN_LINES   = false;
var PP_CUSTOM_NOISE = true; //====
var PP_TONE_MAPPING = false;
var PP_BOKEH        = true; //====
var PP_VIGNETTE     = true; //====

if(WINDOW_HEIGHT > WINDOW_WIDTH) {
    MOBILE = true;
    WINDOW_HEIGHT_MOBILE = WINDOW_HEIGHT/2;
    WINDOW_WIDTH_MOBILE = WINDOW_WIDTH/2;
    WINDOW_PIXEL_RATIO = WINDOW_PIXEL_RATIO/2;
    POST_PROCESSING = false;
    $('body').addClass('mobile');
}

if(WINDOW_PIXEL_RATIO > 2) WINDOW_PIXEL_RATIO = 2;

var Intersects;
/* Fog specific vars BEGIN */
/*var params = {
    fogNearColor: 0xfc4848,
    fogHorizonColor: 0xe4dcff,
    fogDensity: 0.0025,
    fogNoiseSpeed: 100,
    fogNoiseFreq: .0012,
    fogNoiseImpact: .5
};*/
var params = {
    fogNearColor: 0xffffff,
    fogHorizonColor: 0xd3d3d3,
    fogDensity: 0.0011,
    fogNoiseSpeed: 100,
    fogNoiseFreq: 0.0036,
    fogNoiseImpact: 0.33
};
var Shaders_ = [];
const ModifyShader_ = (s) => {
    Shaders_.push(s);
    s.uniforms.fogTime = {value: 0.0};

    s.uniforms.fogNearColor = { value: new THREE.Color(params.fogNearColor) };
    s.uniforms.fogNoiseFreq = { value: params.fogNoiseFreq };
    s.uniforms.fogNoiseSpeed = { value: params.fogNoiseSpeed };
    s.uniforms.fogNoiseImpact = { value: params.fogNoiseImpact };
    s.uniforms.time = { value: 0 };
}
/* Fog specific vars END */

// Loaders

/*const Raycaster = new THREE.Raycaster();
var Intersects;
const loadingManager = new THREE.LoadingManager();
const listener = new THREE.AudioListener();
const audioLoader = new THREE.AudioLoader(loadingManager);*/

/* Helper vars for post processing ----------------------------------------------------------*/
var materials = {};
const ENTIRE_SCENE = 0;
const BLOOM_SCENE = 1;

/* Scene vars */
var MinAltMods = [
    'terrain'
];
var GrdZedAlt = 0;
var ControlsLocked = false;
var sceneSize = 1000;

const rtFov = 70;
const rtAspect = sceneSize / sceneSize;
const rtNear = 0.1;
const rtFar = 5;

/* base fragmentShader */
/*

let baseFragment = "uniform float time;\n" +
                "uniform vec2 resolution;\n" +
                "void main()\t{\n" +
                "    gl_FragColor = vec4(0.5, 0.7, 0.2, 1.0);\n" +
                "}";
let baseVertex = "" +
    "varying vec3 vUv; \n" +
    "\n" +
    "    void main() {\n" +
    "      vUv = position; \n" +
    "\n" +
    "      vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);\n" +
    "      gl_Position = projectionMatrix * modelViewPosition; \n" +
    "    }";

*/

/**
 * Templates
 * =====================================================================================================================
 */
var FREE_BUILD = false;

// Use https://onlinejsontools.com/minify-json to minify the object strings
var templates = [
    {
        template: 'images/templates/template0.jpg',
        matrix: {}
    },
    {
        template: 'images/templates/template1.jpg',
        matrix: {"80.95":{"-120.00":1,"-110.00":1,"-100.00":1,"-90.00":1,"-80.00":1},"32.95":{"-120.00":1,"-110.00":1,"-100.00":1,"-90.00":1,"-80.00":1,"-10.00":1,"0.00":1,"10.00":1,"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"70.00":1},"8.95":{"-120.00":1,"-110.00":1,"-100.00":1,"-90.00":1,"-80.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1,"-10.00":1,"70.00":1,"80.00":1,"90.00":1,"100.00":1,"110.00":1},"44.95":{"-120.00":1,"-110.00":1,"-100.00":1,"-90.00":1,"-80.00":1,"50.00":1,"60.00":1,"70.00":1,"80.00":1,"90.00":1,"-30.00":1,"-20.00":1,"-10.00":1,"0.00":1,"10.00":1},"56.95":{"70.00":1,"80.00":1,"90.00":1,"100.00":1,"110.00":1,"-120.00":1,"-110.00":1,"-100.00":1,"-90.00":1,"-80.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1,"-10.00":1},"20.95":{"50.00":1,"60.00":1,"70.00":1,"80.00":1,"90.00":1,"-30.00":1,"-20.00":1,"-10.00":1,"0.00":1,"10.00":1,"-120.00":1,"-110.00":1,"-100.00":1,"-90.00":1,"-80.00":1},"68.95":{"-120.00":1,"-110.00":1,"-100.00":1,"-90.00":1,"-80.00":1}}
    },
    {
        template: 'images/templates/template2.jpg',
        matrix : {"68.95":{"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"-60.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1},"56.95":{"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"-60.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1},"44.95":{"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"-60.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1},"32.95":{"40.00":1,"50.00":1,"60.00":1,"70.00":1,"80.00":1,"-80.00":1,"-70.00":1,"-60.00":1,"-50.00":1,"-40.00":1},"20.95":{"60.00":1,"70.00":1,"80.00":1,"90.00":1,"100.00":1,"-100.00":1,"-90.00":1,"-80.00":1,"-70.00":1,"-60.00":1},"164.95":{"-60.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1,"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"-10.00":1,"0.00":1,"10.00":1},"8.95":{"-120.00":1,"-110.00":1,"-100.00":1,"-90.00":1,"-80.00":1,"0.00":1,"10.00":1,"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"70.00":1,"80.00":1,"-40.00":1,"-30.00":1,"-20.00":1,"-10.00":1,"-70.00":1,"-60.00":1,"-50.00":1,"90.00":1,"100.00":1,"110.00":1,"120.00":1},"128.95":{"-80.00":1,"-70.00":1,"-60.00":1,"-50.00":1,"-40.00":1,"40.00":1,"50.00":1,"60.00":1,"70.00":1,"80.00":1},"212.95":{"-20.00":1,"-10.00":1,"0.00":1,"10.00":1,"20.00":1},"152.95":{"-40.00":1,"-30.00":1,"-20.00":1,"-10.00":1,"0.00":1,"10.00":1,"20.00":1,"30.00":1,"40.00":1},"188.95":{"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"-60.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1},"176.95":{"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"-60.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1},"140.95":{"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"-60.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1},"80.95":{"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"-60.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1},"92.95":{"20.00":1,"30.00":1,"40.00":1,"50.00":1,"60.00":1,"-60.00":1,"-50.00":1,"-40.00":1,"-30.00":1,"-20.00":1},"104.95":{"40.00":1,"50.00":1,"60.00":1,"70.00":1,"80.00":1,"-80.00":1,"-70.00":1,"-60.00":1,"-50.00":1,"-40.00":1},"116.95":{"60.00":1,"70.00":1,"80.00":1,"90.00":1,"100.00":1,"-100.00":1,"-90.00":1,"-80.00":1,"-70.00":1,"-60.00":1},"200.95":{"0.00":1,"10.00":1,"20.00":1,"30.00":1,"40.00":1,"-40.00":1,"-30.00":1,"-20.00":1,"-10.00":1}}
    }
];
var LOAD_STRING;
var BRICK_COLOUR = "rgb(174,6,6)";
var MODE = 'building';