/**
 * Player character
 */
import * as THREE from './3rdParty/three.module.js'

import {GLTFLoader} from './3rdParty/loaders/GLTFLoader.js'
import {DRACOLoader} from './3rdParty/loaders/DRACOLoader.js'

const gltfLoaderDraco = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath( 'js/3rdParty/loaders/draco/gltf/' );
dracoLoader.setDecoderConfig({type:'js'});
gltfLoaderDraco.setDRACOLoader( dracoLoader );

export const creature = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                var that = this;
                this.clock = new THREE.Clock();
                this.f = 0;
                this.isMoving = true;
                this.M3 = new THREE.Matrix3();
                this.M4 = new THREE.Matrix4();
                this.n  = new THREE.Vector3(); // normal,
                this.n0 = new THREE.Vector3(0,1 ,0); // normal, first up
                this.b  = new THREE.Vector3(); // binormal
                this.p  = new THREE.Vector3();
                this.axisX = new THREE.Vector3(1,0,0);
                this.chooseFromAnimations = [1,2];
                this.randomElement = 2;

                this.LoadModel();
                this.LoadPath();
            }

            update(t) {

                let check = true;
                if(this.model && check) {
                    let delta = this.clock.getDelta();
                    if (this.mixer) this.mixer.update(delta);

                    if (this.isMoving) {
                        if (this.f === 0 || this.f > 1) {
                            this.n.copy(this.n0);
                            this.f = 0; // loop
                        }

                        //this.f += 0.0004;
                        //this.f += 0.00018;
                        //this.f += delta / 100; // cat
                        this.f += delta / 60; // cat2

                        this.t = this.curve.getTangent(this.f);
                        this.b.crossVectors(this.t, this.n);
                        this.n.crossVectors(this.t, this.b.negate());

                        this.M3.set(-this.t.x, this.n.x, this.b.x,
                            -this.t.y, this.n.y, this.b.y,
                            -this.t.z, this.n.z, this.b.z);
                        this.M4 = this.setFromMatrix3(this.M4, this.M3);

                        this.p = this.curve.getPoint(this.f);

                        this.box.setRotationFromMatrix(this.M4);
                        this.box.position.set(this.p.x, this.p.y, this.p.z);
                    }
                }
            }

            setFromMatrix3(m4, m3) {
                this.me = m3.elements;
                m4.set(
                    this.me[ 0 ], this.me[ 3 ], this.me[ 6 ], 0,
                    this.me[ 1 ], this.me[ 4 ], this.me[ 7 ], 0,
                    this.me[ 2 ], this.me[ 5 ], this.me[ 8 ], 0,
                    0, 0, 0, 1
                );
                return m4;
            }

            LoadPath() {
                let yAxis = 10;
                const somePoints = [
                    new THREE.Vector3(-260, yAxis,-300),
                    new THREE.Vector3(0, yAxis,-180),

                    new THREE.Vector3(300, yAxis,-300),
                    new THREE.Vector3(230, yAxis,0),

                    new THREE.Vector3(300, yAxis,300),
                    new THREE.Vector3(0, yAxis,280),

                    new THREE.Vector3(-250, yAxis,300),
                    new THREE.Vector3(-260, yAxis,0),
                ];

                const p = new THREE.Points(new THREE.BufferGeometry().setFromPoints(somePoints), new THREE.PointsMaterial({color: "red", size: 10}));
                //this.currentScene.scene.add(p);

                this.curve = new THREE.CatmullRomCurve3(somePoints);
                this.curve.closed = true;

                const points = this.curve.getPoints(100);
                const line = new THREE.LineLoop( new THREE.BufferGeometry().setFromPoints(points), new THREE.LineBasicMaterial({color: 0xffffaa}));
                //this.currentScene.scene.add(line);
            }

            LoadModel() {
                let that        = this;
                let path        = "./models/creature/lizard/";
                let modelFile   = "scene.gltf";
                //let modelFile   = "kattiec.glb";
                let file        = path + modelFile;

                gltfLoaderDraco.load(file, function (gltf) {
                    that.model = gltf.scene;
                    that.mixer = new THREE.AnimationMixer( that.model );

                    that.model.traverse( function ( object ) {
                        if (object.isMesh) {
                            object.receiveShadow = true;
                            object.castShadow = true;
                            object.material.side = THREE.FrontSide;
                            object.material.onBeforeCompile = ModifyShader_;
                        }
                    });

                    that.model.position.set(0, 0, 0);
                    //that.model.scale.set(0.075, 0.075, 0.075);
                    that.model.scale.set(0.1, 0.1, 0.1);
                    that.model.rotation.y = -Math.PI / 2;
                    that.model.name = 'creature';

                    that.loaded = true;

                    that.box = new THREE.Mesh(
                        new THREE.BoxBufferGeometry(1, 1, 1, 1, 1, 1),
                        new THREE.MeshBasicMaterial({transparent:true, opacity:0.0})
                    );
                    that.box.add(that.model);
                    that.currentScene.scene.add(that.box);

                    that.currentclip = gltf.animations[that.randomElement];
                    that.currentAction = that.mixer.clipAction(that.currentclip).play();

                    that.triggerRandomPose(gltf.animations, null);

                    LOADED++;
                });
            }

            triggerRandomPose(animations, reset) {
                let that = this;
                const array = [...this.chooseFromAnimations];
                const index = array.indexOf(this.randomElement);
                if (index > -1)
                    array.splice(index, 1);

                this.randomElement = array[Math.floor(Math.random() * array.length)];

                if(typeof this.currentAction !== 'undefined' && reset !== null) {

                    if(this.randomElement === 0 || this.randomElement === 1)
                        this.isMoving = false;
                    else
                        this.isMoving = true;

                    this.currentAction.fadeOut(0.5);

                    let clip = animations[this.randomElement];
                    let action = this.mixer.clipAction( clip );

                    action.reset();
                    action.fadeIn(0.5);
                    action.play();
                    this.currentAction = action;
                }

                setTimeout(function() {
                    that.triggerRandomPose(animations, true);
                }, that.randomIntFromInterval(5000, 10000));
            }

            randomIntFromInterval(min, max) {
                return Math.floor(Math.random() * (max - min + 1) + min)
            }
        }
    }
})();