/**
 * Setup the lighting for the scene
 */
import * as THREE from './3rdParty/three.module.js';
import {Lensflare, LensflareElement} from './3rdParty/effects/Lensflare.js';

export const light = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {

                // Main Scene light=====================================================================================
                const TextureLoader = new THREE.TextureLoader();
                this.light = new THREE.DirectionalLight(0xffffff, ((!captureTemplate) ? 4.0 : 1.0));
                this.light.castShadow = true;
                this.light.shadow.mapSize.width = 1024; // default
                this.light.shadow.mapSize.height = 1024; // default
                this.light.shadow.camera.left = -500;
                this.light.shadow.camera.right = 500;
                this.light.shadow.camera.top = 400;
                this.light.shadow.camera.bottom = -400;
                this.light.shadow.camera.near = -200; // default
                this.light.shadow.camera.far = 1000; // default

                /*this.shadowHelper = new THREE.CameraHelper(this.light.shadow.camera);
                this.currentScene.scene.add(this.shadowHelper);*/

                this.light.position.set(250, 250, 250);
                this.light.name = 'directionalLight';
                this.currentScene.scene.add(this.light);

                // LensFlare for main light
                this.textureFlare0 = TextureLoader.load("images/sky/fx/lensflare1.png");
                this.textureFlare1 = TextureLoader.load("images/sky/fx/hexangle.png");
                const lensFlare = new Lensflare();
                lensFlare.addElement(new LensflareElement(this.textureFlare0, 128, 0));
                lensFlare.addElement(new LensflareElement(this.textureFlare1, 32, 0.2));
                lensFlare.addElement(new LensflareElement(this.textureFlare1, 64, 0.5));
                lensFlare.addElement(new LensflareElement(this.textureFlare1, 256, 0.9));
                lensFlare.name = 'lensflare';
                this.light.add(lensFlare);
                lensFlare.position.set(sceneSize/5, sceneSize/5, sceneSize/5);

                // Light 2 to emulate bouncing light====================================================================
                this.light2 = new THREE.DirectionalLight(0xffffff, 1.0);
                this.light2.castShadow = false;

                this.light2.position.set(-250, 250, 250);
                this.light2.name = 'directionalLight2';
                this.currentScene.scene.add(this.light2);

                // ShadowLight==========================================================================================
                const targetObject = new THREE.Object3D();
                targetObject.position.set(-180, 125, 0);
                targetObject.name = 'shadowTarget';
                this.currentScene.scene.add(targetObject);

                this.shadowLight = new THREE.DirectionalLight(0xffffff, 4.0);
                this.shadowLight.castShadow = true;
                this.shadowLight.shadow.mapSize.width = 512; // default
                this.shadowLight.shadow.mapSize.height = 512; // default
                this.shadowLight.shadow.camera.left = -125;
                this.shadowLight.shadow.camera.right = 125;
                this.shadowLight.shadow.camera.top = 125;
                this.shadowLight.shadow.camera.bottom = -125;
                this.shadowLight.shadow.camera.near = 0; // default
                this.shadowLight.shadow.camera.far = 530; // default

                /*this.shadowHelper = new THREE.CameraHelper(this.shadowLight.shadow.camera);
                this.currentScene.scene.add(this.shadowHelper);*/

                this.shadowLight.position.set(230, 125, 0);
                this.shadowLight.name = 'shadowLight';
                this.shadowLight.target = targetObject;
                this.currentScene.scene.add(this.shadowLight);

                LOADED++;
            }
        }
    }
})();