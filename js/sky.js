/**
 * Setup the skybox for the scene
 */
import * as THREE from './3rdParty/three.module.js';
const TextureLoader = new THREE.TextureLoader();
export const sky = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                // Specify path of current skybox
                let sbName = "Creek";
                let fPath = "images/sky/box/" + sbName + "/";

                let materialArray = [];
                let texture_ft = TextureLoader.load( fPath+'posx.jpg');
                let texture_bk = TextureLoader.load( fPath+'negx.jpg');
                let texture_up = TextureLoader.load( fPath+'posy.jpg');
                let texture_dn = TextureLoader.load( fPath+'negy.jpg');
                let texture_rt = TextureLoader.load( fPath+'posz.jpg');
                let texture_lf = TextureLoader.load( fPath+'negz.jpg');

                let showFog = false;
                if(BETTER_FOG || BETTER_FOG2) {
                    showFog = true;
                }

                materialArray.push(new THREE.MeshBasicMaterial( { map: texture_ft }));
                materialArray.push(new THREE.MeshBasicMaterial( { map: texture_bk }));
                materialArray.push(new THREE.MeshBasicMaterial( { map: texture_up }));
                materialArray.push(new THREE.MeshBasicMaterial( { map: texture_dn }));
                materialArray.push(new THREE.MeshBasicMaterial( { map: texture_rt }));
                materialArray.push(new THREE.MeshBasicMaterial( { map: texture_lf }));

                for (let i = 0; i < 6; i++) {
                    materialArray[i].side = THREE.BackSide;
                    materialArray[i].fog = showFog;
                    materialArray[i].onBeforeCompile = ModifyShader_;
                }

                let skyboxGeo = new THREE.BoxGeometry( sceneSize, sceneSize, sceneSize);
                this.envMap = new THREE.Mesh( skyboxGeo, materialArray );
                //this.envMap.position.set(0, sceneSize/4-1, 0);
                this.envMap.position.set(0, 40, 0);
                this.envMap.name = 'skybox';

                if(BETTER_FOG || BETTER_FOG2) {
                    this.envMap.material.onBeforeCompile = ModifyShader_;
                }

                this.currentScene.scene.add(this.envMap);

                LOADED++;
            }
        }
    }
})();