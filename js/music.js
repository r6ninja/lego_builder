/**
 * Setup the lighting for the scene
 */
import * as THREE from './3rdParty/three.module.js';
import {TWEEN} from './3rdParty/tween.module.min.js';
export const music = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {

                let that = this;
                this.time = 0;
                this.volume = {x:1};
                this.initiated = false;

                this.listener = new THREE.AudioListener();
                this.actionedPlay = false;
                this.actionedFade = true;
                //this.currentScene.camera.add(listener);

                this.musicTrack = new THREE.Audio(this.listener);
                const audioLoader = new THREE.AudioLoader();
                audioLoader.load( './sounds/suit-up-aj-hochhalter.mp3', function( buffer ) {
                    that.musicTrack.setBuffer( buffer );
                    that.musicTrack.setLoop( true );
                    that.musicTrack.setVolume( 1.0 );
                });

                this.events();

                LOADED++;
            }

            update(e, inview) {

                this.render = inview;
                if(this.tweening) {
                    TWEEN.update(e);
                }

                if(MUSIC) {
                    if (!this.initiated && this.render !== false) {
                        this.musicTrack.play();
                        this.initiated = true;
                    }

                    if(!this.render && this.actionedFade) {
                        this.actionedFade = false;
                        this.fadeOut();
                    } else if(this.render && !this.actionedFade) {
                        this.actionedFade = true;
                        this.fadeIn();
                    }
                }
            }

            events() {
                let that = this;
                document.addEventListener("mousedown", function(event) { that.onDocumentMousedown(that, event); }, false);
                document.addEventListener("keydown", function(event) { that.onDocumentKeyDown(that, event); }, false);
                window.addEventListener("pauseMusic", function(event) {
                    if(that.render) {
                        that.fadeOut();
                    }
                }, false);
                window.addEventListener("playMusic", function(event) {
                    if(that.render) {
                        that.fadeIn();
                    }
                }, false);
            }

            onDocumentMouseMove() {
                if(!this.actionedPlay && this.initiated) {
                    this.musicTrack.pause();
                    this.musicTrack.play();
                    this.actionedPlay = true;
                }
            }

            onDocumentMousedown() {
                if(!this.actionedPlay && this.initiated) {
                    this.musicTrack.pause();
                    this.musicTrack.play();
                    this.actionedPlay = true;
                }
            }

            onDocumentKeyDown() {
                if(!this.actionedPlay && this.initiated) {
                    this.musicTrack.pause();
                    this.musicTrack.play();
                    this.actionedPlay = true;
                }
            }

            fadeOut() {
                let that = this;

                if(typeof this.fadeInTween !== 'undefined')
                    this.fadeInTween.stop();

                //this.time = 0;
                this.tweening = true;
                this.volume = {x:1};
                this.fadeOutTween = new TWEEN.Tween(this.volume).to({
                    x: 0
                }, 500).onUpdate(function(fade) {
                    that.musicTrack.setVolume(fade.x);
                }).onComplete(function() {
                    that.musicTrack.pause();
                    that.tweening = false;
                }).start();
            }

            stop() {
                this.musicTrack.pause();
            }

            play() {
                this.musicTrack.play();
            }

            fadeIn() {
                let that = this;

                if(typeof this.fadeOutTween !== 'undefined')
                    this.fadeOutTween.stop();

                //this.time = 0;
                this.tweening = true;
                this.musicTrack.play();
                this.musicTrack.setVolume(0);
                this.volume = {x:0};
                this.fadeInTween = new TWEEN.Tween(this.volume).to({
                    x: 1
                }, 1000).onUpdate(function(fade) {
                    that.musicTrack.setVolume(fade.x);
                }).onComplete(function() {
                    that.tweening = false;
                }).start();
            }
        }
    }
})();