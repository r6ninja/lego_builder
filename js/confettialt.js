/**
 * Player character
 */
import ExplosionConfetti  from './3rdParty/three.confetti.explode.js'

export const confettiAlt = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                this.isShooting = false;
                this.events();
                LOADED++;
            }

            events() {
                var that = this;
                window.addEventListener('complete', function () {
                    if(!that.isShooting)
                        that.shootConfetti();
                }, false);
            }

            shootConfetti() {
                let that = this;
                this.confetti = new ExplosionConfetti({
                    rate: 2, // percent of explosion in every tick - smaller is fewer - be careful, larger than 10 may crash your browser!
                    amount: 200, // max amount particle of an explosion
                    radius: 400, // max radius of an explosion
                    areaWidth: 500, // width of the area
                    areaHeight: 500, // height of the area
                    fallingHeight: 400, // start exploding from Y position
                    fallingSpeed: 1, // max falling speed
                    /*colors: [
                        0xffffff,
                        0xff0000,
                        0xffff00,
                        0x00ff00,
                        0x0000ff
                    ],*/
                    colors: [
                        0x5caf38,
                        0x000000,
                        0xFFFFFF,
                        0xd59f24
                    ]
                    //colors: [0xffffff, 0xff0000, 0xffff00, 0x00ff00, 0x0000ff] // random colors
                });
                this.currentScene.scene.add(this.confetti.object);

                this.isShooting = true;
                setTimeout(function() {
                    that.isShooting = false;
                    that.confetti.dispose();
                    console.log('Confetti finished.');
                }, (1000*20));


            }

            update() {
                if(this.confetti) this.confetti.update();
            }
        }
    }
})();