/**
 * Player character
 */
import * as THREE from './3rdParty/three.module.js'
const TextureLoader = new THREE.TextureLoader();
export const templateScreen = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                var that = this;

                this.LoadTemplate();
            }

            LoadTemplate() {
                let that = this;

                let geometry = new THREE.PlaneBufferGeometry(300, 300, 5, 5);
                let material = new THREE.MeshBasicMaterial({
                    transparent:true,
                    //map: TextureLoader.load('images/templates/template4.png', function ( texture ) {
                    map: TextureLoader.load('images/templates/template5.png', function ( texture ) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.repeat.set(1, 1);
                    }),
                });
                this.template = new THREE.Mesh(geometry, material);
                this.template.receiveShadow = true;
                this.template.position.set(-281, 122, 0);
                this.template.rotation.y = Math.PI / 2;
                this.template.name = 'templateScreen';

                if(BETTER_FOG || BETTER_FOG2) {
                    this.template.material.onBeforeCompile = ModifyShader_;
                }

                this.currentScene.scene.add(this.template);
                LOADED++;
            }
        }
    }
})();