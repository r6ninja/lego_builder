import { NoBlending, ShaderMaterial, Uniform } from "../../three.module.js";

import { fragmentShader } from "./glsl/copy/shader.frag.js";
import { vertexShader } from "./glsl/common/shader.vert.js";

/**
 * A simple copy shader material.
 */

export class CopyMaterial extends ShaderMaterial {

	/**
	 * Constructs a new copy material.
	 */

	constructor() {

		super({

			type: "CopyMaterial",

			uniforms: {
				inputBuffer: new Uniform(null),
				opacity: new Uniform(1.0)
			},

			fragmentShader,
			vertexShader,

			blending: NoBlending,
			depthWrite: false,
			depthTest: false

		});

		/** @ignore */
		this.toneMapped = false;

	}

}
