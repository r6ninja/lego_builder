/**
 * Setup the controls for the scene
 */
import * as THREE from './3rdParty/three.module.js';
import {OrbitControls} from './3rdParty/controls/OrbitControls.js';
export const orbitControls = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                let that = this;

                this.controls = new OrbitControls(this.currentScene.camera, this.currentScene.renderer.domElement);
                this.controls.minDistance = 0.1;
                this.controls.maxDistance = 600;
                this.controls.minPolarAngle = 0; // radians
                //this.controls.maxPolarAngle = 1.4; // radians
                this.controls.maxPolarAngle = (!captureTemplate) ? 1.4 : 1.8; // radians
                this.controls.minAzimuthAngle = - Infinity; // radians
                this.controls.maxAzimuthAngle = Infinity; // radians
                this.controls.enableDamping = true;
                this.controls.dampingFactor = 0.02;
                this.controls.rotateSpeed = 0.15;
                this.controls.screenSpacePanning = false;
                //this.controls.target.set(2.8428801558538246, 18.075174822304778, 9.958827597916272);
                (!captureTemplate) ? this.controls.target.set(-74.63, 2.93, 98.05) : this.controls.target.set(-250, 122, 0);

                if(!captureTemplate) {
                    this.pan = new THREE.Vector3();
                    let minPan = new THREE.Vector3(-300, -0, -300);
                    let maxPan = new THREE.Vector3(300, 0, 300);
                    let _v = new THREE.Vector3();

                    this.controls.addEventListener("change", function () {
                        _v.copy(that.controls.target);
                        that.controls.target.clamp(minPan, maxPan);
                        _v.sub(that.controls.target);
                        that.currentScene.camera.position.sub(_v);
                    });
                }

                window.controls = this.controls;

                // Initialise object specific events
                this.events();

                // Start the scene animation
                this.animateMe = function() {
                    that.animate();
                };

                LOADED++;
            }

            // Object events
            events() {
                let that = this;
                // Add event listeners
                window.addEventListener('lockControls', function () {
                    //that.controls.lock();
                }, false);

                this.controls.addEventListener('unlock', function () {
                    const event = new Event('unLockControls');
                    window.dispatchEvent(event);
                });
            }

            // Updates the variables for every frame iteration
            update() {
                this.controls.update();
            }

            // Calls the applicable functions to animate the scene
            animate() {
                requestAnimationFrame(this.animateMe);
                this.update();
            }
        }
    };
})();