import {WEBGL} from './3rdParty/WebGL.js';
import * as THREE from './3rdParty/three.module.js';
import {scene} from './scene.js';
import {instructions} from './instructions.js';
import {events} from './events.js';
import {orbitControls} from './orbitControls.js';
import {light} from './light.js';
import {sky} from './sky_dome.js';
import {terrain} from './terrain.js';
import {screen} from './screen.js';
import {shadowScreen} from './shadowScreen.js';
//import {templateScreen} from './templateScreen.js';
import {confetti} from './confetti.js';
import {confettiAlt} from './confettialt.js';
import {lego_grid} from './lego_grid.js';
import {spotlight} from './spotlight.js';
import {creature} from './creature-trex.js';
import {creatureFlying} from './creature-pterodactyl.js';
import {effects} from './effects.js';

//import {music} from './music.js';
import {postShaders} from './postShaders_new.js';
import {BlendFunction, KernelSize} from "./3rdParty/effects_new/index.js";
import {stats} from './stats.js';

if(postShaders+light+instructions+sky+events+terrain+shadowScreen+screen+confetti+confettiAlt+lego_grid+spotlight+creature+creatureFlying+orbitControls+stats+effects) console.log('All inc.');

if (WEBGL.isWebGLAvailable()) {

    var scenesRendered = {
        0:false,
        1:false,
        2:false
    };
    var sceneContents = [];
    if(!captureTemplate) {
        sceneContents[0] = [
            {'postShaders': []},
            {'instructions': []},
            {'events': []},
            {'orbitControls': []},
            {'light': []},
            {'sky': []},
            {'terrain': []},
            {'shadowScreen': []},
            //{'templateScreen': []},
            //{'confetti':[]},
            {'confettiAlt': []},
            {'lego_grid': []},
            {'spotlight': []},
            //{'creature': []},
            //{'creatureFlying': []},
            {'effects': []},
            {'stats': []}
        ];
        if(QUALITY_LEVEL !== 'low') {
            sceneContents[0].push({'creature': []});
            sceneContents[0].push({'creatureFlying': []});
        }
    } else {
        sceneContents[0] = [
            {'instructions': []},
            {'orbitControls': []},
            {'light': []},
            {'screen':[]},
            {'lego_grid': []},
            {'stats': []}
        ];
    }

    var scenesArray=[];

    let can = document.getElementById('threejs');
    var renderer = new THREE.WebGLRenderer({
        canvas: can,
        antialias: !POST_PROCESSING,
        alpha: true,
        powerPreference: "high-performance",
        stencil: !POST_PROCESSING,
        depth: !POST_PROCESSING
    });
    renderer.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    renderer.setPixelRatio(WINDOW_PIXEL_RATIO);
    renderer.autoClear = false;
    //renderer.gammaFactor = 2.2;
    //renderer.outputEncoding = THREE.sRGBEncoding;
    //renderer.outputEncoding = THREE.LinearEncoding;
    //renderer.toneMapping = THREE.ReinhardToneMapping;
    renderer.physicallyCorrectLights = true;
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    // Setup the scenes and start animating them
    scenesArray[0] = new scene.Main(sceneSize*2.1, 'container0', renderer, {
        gui: PP_GUI,
        bloom: {on:PP_BLOOM, settings:{height:360, luminanceSmoothing:0.4, luminanceThreshold:0.0, resolutionScale:1.0, intensity:1.0, kernelSize:KernelSize.MEDIUM, blendFunction:BlendFunction.SCREEN}},
        bloomSelect: {on:PP_BLOOM_SELECT, settings:{blendFunction: BlendFunction.COLOR_DODGE, kernelSize: KernelSize.MEDIUM, intensity: 3.5, luminanceThreshold: 0.1, luminanceSmoothing: 0.2, height: 1080}},
        scanlines: {on:PP_SCAN_LINES, settings:{blendFunction : BlendFunction.OVERLAY}},
        customNoise:{on:PP_CUSTOM_NOISE, settings:{blendFunction : BlendFunction.ALPHA, premultiply: true, opacity: 0.75, fullScreen: true, fadeBottom: false, fadeTop:false, emptyFadeTop:false, emptyFadeBottom:true, offSet: true}},
        toneMapping:{on:PP_TONE_MAPPING},
    //bokeh:{on:true, settings:{blendFunction: BlendFunction.NORMAL, focusDistance: 0.071, focalLength: 0.1368, bokehScale: 1.5}},
        bokeh:{on:PP_BOKEH, settings:{blendFunction: BlendFunction.NORMAL, focusDistance: 0.159, focalLength: 0.2692, bokehScale: 1.5}},
        vignette:{on:PP_VIGNETTE}
    }, false, false);

    for(let c=0;c<scenesArray.length;c++) {
        for (let t = 0; t < sceneContents[c].length; t++) {
            let constructorName = Object.keys(sceneContents[c][t])[0];
            sceneContents[c][t][constructorName][0] = eval('new ' + constructorName + '.Main(scenesArray[c])');
        }
    }

    window.scene  = scenesArray[0].scene;
    window.camera = scenesArray[0].camera;
    window.THREE  = THREE;

} else {
    const warning = WEBGL.getWebGLErrorMessage();
    document.getElementById("container").appendChild(warning);
}

function renderScene(e) {

    // Animate main scene
    renderer.setScissorTest( false );
    renderer.clear();
    renderer.setScissorTest( true );
    if(sceneContents[0][0]['postShaders'])
        sceneContents[0][0]['postShaders'][0].clear(false);

    for(let m=0;m<scenesArray.length;m++) {
        const rect = document.getElementById('container' + m).getBoundingClientRect();

        let renderMe = true;

        // check if it's offscreen. If so skip it
        if (rect.bottom <= 0 || rect.top > renderer.domElement.clientHeight ||
            rect.right < 0 || rect.left > renderer.domElement.clientWidth) {
            renderMe = false;
        }

        if (renderMe) {
            const width = rect.right - rect.left;
            const height = rect.bottom - rect.top;
            const left = rect.left;
            const bottom = renderer.domElement.clientHeight - rect.bottom;

            renderer.setScissor(left, bottom, width, height);
        }

        // Render everything at least once to speed up the page further down
        if(!scenesRendered[m]) {
            renderMe = true;
            scenesRendered[m] = true;
        }

        // Animate nodes
        for (let t = 0; t < sceneContents[m].length; t++) {
            let constructorName = Object.keys(sceneContents[m][t])[0];
            if (typeof sceneContents[m][t][constructorName][0].update != 'undefined') {
                sceneContents[m][t][constructorName][0].update(e, renderMe, rect.top, rect.bottom, m);
            }
        }

        if (renderMe) {
            scenesArray[m].update(e, rect.top, rect.bottom, m);
        }
    }

    if(SIMULATE) {
        setTimeout( function() {
            requestAnimationFrame( renderScene );
        }, 1000 / 30 );
    } else {
        requestAnimationFrame(renderScene);
    }
}

// Let's GO!
renderScene();

// Handle loading
var loadingVar = setInterval(loading, 20);
function loading() {
    if(LOADED >= (sceneContents[0].length)) {
        stopLoading();
    }
    $('#loader_text').html(((LOADED/(sceneContents[0].length))*100).toFixed(0)+'%');
}
function stopLoading() {
    clearInterval(loadingVar);
    $('body').addClass('loaded');
}

$(window).focus(function() {
    WINDOW_ACTIVE = true;
    if(MUSIC) {
        //sceneContents[0][4]['music'][0].play();
    }
});

$(window).blur(function() {
    WINDOW_ACTIVE = false;
    //sceneContents[0][4]['music'][0].stop();
});