$(document).ready(function() {
    $('#header-menu-btn').click(function(e) {
        $('html').toggleClass('is-menu-visible');
        $(this).toggleClass('is-selected');
    });

    $('#soundBars').click(function () {
        $('#soundBars .bar').each(function() {
            $(this).toggleClass('playing');
            $(this).toggleClass('hidebar');
        });
        $(this).toggleClass('playing');
        $('.play-btn').toggleClass('playing');

        MUSIC = !MUSIC;

        if(!MUSIC) {
            const event = new Event('pauseMusic');
            window.dispatchEvent(event);
        } else {
            const event = new Event('playMusic');
            window.dispatchEvent(event);
        }
    });

    $('.header-link').click(function(e) {
        $.scrollify.move($(this).attr("href"));
        $('html').removeClass('is-menu-visible');
        $('#header-menu-btn').removeClass('is-selected');
    });

    $('.templates li.sTemp').click(function(e) {
        $('.templates li.sTemp').removeClass('mode');
        $(this).addClass('mode');
        e.preventDefault();
        let id = $(this).data('id');
        FREE_BUILD = (id === 0);
        const event = new CustomEvent("changeT", {
            "detail": {id:id}
        });
        window.dispatchEvent(event);
    });

    $('.models li.bricks').click(function(e) {
        e.preventDefault();
        let id = $(this).data('id');
        const event = new CustomEvent("changeModel", {
            "detail": {id:id}
        });
        window.dispatchEvent(event);
    });

    $('.loadBricksButton').click(function(e) {
        e.preventDefault();
        $('.loadBrickString').fadeIn('medium', function() {
            $('#loadString').unbind('click').click(function(e) {
                e.preventDefault();
                LOAD_STRING = $('#loadBrickCoOrds').val();
                const event = new Event('loadBricks');
                window.dispatchEvent(event);
                $(this).parents('.objectForm').fadeOut();
            });
        });
    });

    $('.copyBricksButton').click(function(e) {
        e.preventDefault();
        const event = new Event('saveBricks');
        window.dispatchEvent(event);

        $('.copyBrickString').fadeIn('medium', function() {
            $('#copyBrickCoOrds').val(JSON.stringify(brickMmap));
            $('#copyToClipboard').unbind('click').click(function(e) {
                e.preventDefault();
                copy('copyBrickCoOrds');

                alert('Bricks string has been copied to clipboard!');
                $(this).parents('.objectForm').fadeOut();
            });
        });
    });

    $('.closeBtn').click(function() {
        $(this).parents('.objectForm').fadeOut();
    });

    $(".objectForm").click(function(){
        $(this).fadeOut();
    }).children().click(function(e) {
        return false;
    });

    // https://www.cssscript.com/color-picker-alpha-selection/
    new Picker({
        parent: document.querySelector('#colourPick'),
        popup: 'top',
        alpha: false,
        onChange: function(color){
            $('#buildBricks').removeClass('mode');
            $('#colourPick').addClass('mode');
            BRICK_COLOUR = "rgb("+color._rgba[0]+","+color._rgba[1]+","+color._rgba[2]+")";
            MODE = 'painting';
            $("html").addClass('painting');
        }
    });

    $('#colourPick').click(function() {
        $('#buildBricks').removeClass('mode');
        $('#colourPick').addClass('mode');
        MODE = 'painting';
        $("html").addClass('painting');
        const event = new Event('switchPaint');
        window.dispatchEvent(event);
    });

    $('#buildBricks').click(function() {
        $('#buildBricks').addClass('mode');
        $('#colourPick').removeClass('mode');
        MODE = 'building';
        $("html").removeClass('painting');
    });
});

function copy(element_id){
    var aux = document.createElement("div");
    aux.setAttribute("contentEditable", true);
    aux.innerHTML = document.getElementById(element_id).value;
    aux.setAttribute("onfocus", "document.execCommand('selectAll',false,null)");
    document.body.appendChild(aux);
    aux.focus();
    document.execCommand("copy");
    document.body.removeChild(aux);
}