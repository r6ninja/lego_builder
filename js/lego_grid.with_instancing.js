/**
 * Player character
 */
import * as THREE from './3rdParty/three.module.js'

import {GridSnapInstance} from './3rdParty/grid/GridSnap.module.Instance.js'
import {GridSnap} from './3rdParty/grid/GridSnap.module.original.js'

import {GLTFLoader} from './3rdParty/loaders/GLTFLoader.js'
import {DRACOLoader} from './3rdParty/loaders/DRACOLoader.js'

const gltfLoaderDraco = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath( 'js/3rdParty/loaders/draco/gltf/' );
dracoLoader.setDecoderConfig({type:'js'});
gltfLoaderDraco.setDRACOLoader( dracoLoader );

export const lego_grid = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                var that = this;
                this.mousePos = null;

                this.LoadModel();
            }

            loadGrid() {
                var that = this;
                this.hoverMesh = new THREE.Mesh(new THREE.SphereGeometry(5.0, 32, 32), new THREE.MeshStandardMaterial({ color: 0x0000ff}));
                this.markerMesh = new THREE.Mesh(new THREE.SphereGeometry(5.0, 32, 32), new THREE.MeshStandardMaterial({ color: 0xff0000}));
                this.snapRadius = 20; // How big radius we search for vertices near the mouse click
                this.plane = new THREE.Mesh(
                    new THREE.PlaneGeometry(sceneSize, sceneSize, 100, 100),
                    new THREE.MeshPhongMaterial({ color: 0x000000, wireframe: true, transparent: true, opacity:0.0 })
                )
                this.plane.position.set(0, 8.95, 0);
                this.plane.rotation.x = -Math.PI / 2;
                this.currentScene.scene.add(this.plane);
            }

            events() {
                let that= this;
                document.addEventListener('pointermove', function (event) {
                    that.mousePos = event;
                    that.snap.mouseMove(event);
                }, false);
                document.addEventListener('pointerdown', function(event) {
                    that.snap.mouseDown(event);
                }, true);
                document.addEventListener('pointerup', function(event) {
                    that.snap.mouseUp(event);
                }, true);
            }

            update() {
                if(this.mousePos !== null) {
                    //this.snap.mouseMove(this.mousePos);
                }
            }

            makeInstances(geometry, material) {

                const instanceCount = material.userData.instanceCount;

                const instanceID = new THREE.InstancedBufferAttribute(
                    new Float32Array( new Array( instanceCount ).fill( 0 ).map( ( _, index ) => index ) ),
                    1
                );

                geometry = new THREE.InstancedBufferGeometry().copy( geometry );
                geometry.addAttribute( 'instanceID', instanceID );
                geometry.maxInstancedCount = instanceCount;

                return geometry;
            }

            LoadModel() {
                let that        = this;
                let path        = "./models/";
                let modelFile   = "lego_brick.glb";
                let file        = path + modelFile;

                gltfLoaderDraco.load(file, function (gltf) {
                    that.model = gltf.scene;
                    that.model.traverse( function ( object ) {
                        if ( object.isMesh) {
                            object.material.onBeforeCompile = ModifyShader_;
                            object.receiveShadow = true;
                            object.castShadow = true;
                            object.material.side = THREE.FrontSide;
                        }
                    });

                    that.model.scale.set(1.26, 1.26, 1.26);
                    that.model.position.y = 8.95;
                    //that.currentScene.scene.add(that.model);

                    that.instanceGeometry = new THREE.InstancedBufferGeometry();
                    that.model.traverse( function ( child ) {
                        if (child.isMesh) {
                            that.instanceGeometry.copy(child.geometry);
                        }
                    });
                    let count = 10;
                    let xDistance = 20;
                    let zDistance  = 40;
                    that.dummy = new THREE.Object3D();
                    that.mchr = new THREE.InstancedMesh(that.instanceGeometry,new THREE.MeshPhongMaterial({
                        color:0xae0606,
                        reflectivity: 1.0,
                        shininess: 200.0,
                        emissive:0x000000,
                        emissiveIntensity:5.0,
                        specular:0x111111,
                        side:THREE.FrontSide
                    }),count*count);
                    that.mchr.frustumCulled = true;
                    that.mchr.castShadow = true;
                    that.mchr.receiveShadow = true;
                    //that.mchr.rotation.x = -Math.PI / 2;
                    that.mchr.scale.set(1, 1, 1);
                    that.mchr.position.set(0, 8.95, 0);

                    for(let i = 0; i < count; i++){
                        for(let j = 0; j < count; j++){
                            that.dummy.position.set(xDistance * i, -5000, zDistance * j);
                            //that.dummy.position.set(xDistance * i, 0, zDistance * j);
                            that.dummy.scale.set(126, 126, 126);
                            that.dummy.rotation.x = -Math.PI / 2;
                            that.dummy.updateMatrix();
                            that.mchr.setMatrixAt(i + j * count, that.dummy.matrix);

                            //let mod = that.model.clone();
                            //mod.position.set(xDistance * i, 8.95, zDistance * j);
                            //that.currentScene.scene.add(mod);
                        }
                    }
                    that.mchr.instanceMatrix.needsUpdate = true;

                    that.loaded = true;

                    that.loadGrid();

                    // Using actual model
                    that.snap = new GridSnap(that.currentScene.scene, that.currentScene.renderer, that.currentScene.camera, that.plane, that.snapRadius, that.model, that.model);

                    // Using Index of instance Mesh
                    //that.snap = new GridSnapInstance(that.currentScene.scene, that.currentScene.renderer, that.currentScene.camera, that.plane, that.snapRadius, 0, that.mchr, that.dummy);

                    that.events();

                    LOADED++;
                });
            }
        }
    }
})();