/**
 * Setup the base prototype scene along with the first scene
 */
import * as THREE from './3rdParty/three.module.js';
const TextureLoader = new THREE.TextureLoader();
import {math} from './common/math.js';
export const terrain = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {

                let physicalMaterial = ['tiles', 'ground', 'snow', 'asphalt', 'asphalt2', 'pebbles', 'terra'];
                let floorType = physicalMaterial[1];
                //let floorType = 'paving';
                //let floorScaleX = 1.0;
                let floorScaleX = 8.0;
                //let floorScaleY = 2.0;
                let floorScaleY = 8.0;
                let floorOffsetX = 0.0;
                //let floorOffsetY = 0.5;
                let floorOffsetY = 0.0;

                let geometry = new THREE.PlaneBufferGeometry(sceneSize*2, sceneSize*2, 256, 256);
                let uvs = geometry.attributes.uv.array;
                geometry.setAttribute( 'uv2', new THREE.BufferAttribute( uvs, 2 ) );
                //geometry.faceVertexUvs[1] = geometry.faceVertexUvs[0]; // 2nd set of UV's for aoMap... IMPORTANT!

                let material = new THREE.MeshPhysicalMaterial({
                    //map: TextureLoader.load('images/'+floorType+'/'+floorType+'.png', function ( texture ) {
                    map: TextureLoader.load('images/'+floorType+'/'+floorType+'.jpg', function ( texture ) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.repeat.set(floorScaleX, floorScaleY);
                        texture.offset.set(floorOffsetX, floorOffsetY);
                    }),
                    displacementMap: TextureLoader.load('images/'+floorType+'/'+floorType+'_bump.png', function ( texture ) {
                    //displacementMap: TextureLoader.load('images/'+floorType+'/'+floorType+'_bump.jpg', function ( texture ) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.repeat.set(floorScaleX, floorScaleY);
                        texture.offset.set(floorOffsetX, floorOffsetY);
                    }),
                    displacementScale: 20.0,
                    //normalMap: TextureLoader.load('images/'+floorType+'/'+floorType+'_normal.png', function ( texture ) {
                    normalMap: TextureLoader.load('images/'+floorType+'/'+floorType+'_normal.jpg', function ( texture ) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.repeat.set(floorScaleX, floorScaleY);
                        texture.offset.set(floorOffsetX, floorOffsetY);
                    }),
                    normalScale: new THREE.Vector2(1.5, 1.5),
                    //roughnessMap: TextureLoader.load('images/'+floorType+'/'+floorType+'_roughness.png', function ( texture ) {
                    roughnessMap: TextureLoader.load('images/'+floorType+'/'+floorType+'_roughness.jpg', function ( texture ) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.repeat.set(floorScaleX, floorScaleY);
                        texture.offset.set(floorOffsetX, floorOffsetY);
                    }),
                    //aoMap: TextureLoader.load('images/'+floorType+'/'+floorType+'_ao.png', function ( texture ) {
                    aoMap: TextureLoader.load('images/'+floorType+'/'+floorType+'_ao.jpg', function ( texture ) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.repeat.set(floorScaleX, floorScaleY);
                        texture.offset.set(floorOffsetX, floorOffsetY);
                    }),
                    aoMapIntensity: 1.0
                });

                this.terrain = new THREE.Mesh(geometry, material);

                this.terrain.receiveShadow = true;
                this.terrain.castShadow = true;
                this.terrain.rotation.x = -Math.PI / 2;
                this.terrain.updateMatrix();
                this.terrain.geometry.applyMatrix4( this.terrain.matrix );
                this.terrain.rotation.set( 0, 0, 0 );
                this.terrain.position.set( 0, -1.6, 0 );
                //this.terrain.scale.set( 1, 1, 0.28 );
                this.terrain.name = 'terrain';
                this.terrain.type = 'solid';

                if(BETTER_FOG || BETTER_FOG2) {
                    this.terrain.material.onBeforeCompile = ModifyShader_;
                }

                this.currentScene.scene.add(this.terrain);

                LOADED++;
            }
        }
    }
})();