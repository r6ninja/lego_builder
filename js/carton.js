/**
 * Player character
 */
import * as THREE from './3rdParty/three.module.js'

import {GLTFLoader} from './3rdParty/loaders/GLTFLoader.js'
import {DRACOLoader} from './3rdParty/loaders/DRACOLoader.js'

const gltfLoaderDraco = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath( 'js/3rdParty/loaders/draco/gltf/' );
dracoLoader.setDecoderConfig({type:'js'});
gltfLoaderDraco.setDRACOLoader( dracoLoader );

export const carton = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                var that = this;

                this.LoadModel();
            }

            LoadModel() {
                let that        = this;
                let path        = "./models/";
                let modelFile   = "carton.glb";
                let file        = path + modelFile;

                gltfLoaderDraco.load(file, function (gltf) {
                    that.model = gltf.scene;
                    that.model.traverse( function ( object ) {
                        if ( object.isMesh) {
                            object.material.onBeforeCompile = ModifyShader_;
                            object.layers.enable( BLOOM_SCENE );
                            object.receiveShadow = true;
                            object.castShadow = true;
                        }
                    });

                    that.model.scale.set(1, 1, 1);
                    that.model.position.y = 8.95;
                    that.currentScene.scene.add(that.model);

                    that.loaded = true;

                    LOADED++;
                });
            }
        }
    }
})();