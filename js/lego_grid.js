/**
 * Player character
 */
import * as THREE from './3rdParty/three.module.js'

import {GridSnap} from './3rdParty/grid/GridSnap.module.original.js'

import {GLTFLoader} from './3rdParty/loaders/GLTFLoader.js'
import {DRACOLoader} from './3rdParty/loaders/DRACOLoader.js'

const gltfLoaderDraco = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath( 'js/3rdParty/loaders/draco/gltf/' );
dracoLoader.setDecoderConfig({type:'js'});
gltfLoaderDraco.setDRACOLoader( dracoLoader );

export const lego_grid = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                var that = this;
                this.mousePos = null;
                this.currentMarker = [];
                this.currentModel = [];
                this.chosenMarker = 0;

                this.LoadModel();
            }

            loadGrid() {
                this.snapRadius = 10; // How big radius we search for vertices near the mouse click
                this.plane = new THREE.Mesh(
                    new THREE.PlaneGeometry(sceneSize, sceneSize, 100, 100),
                    new THREE.MeshBasicMaterial({ color: 0x00FF00, wireframe: true, transparent: true, opacity:0.3 })
                )
                this.plane.position.set(0, 8.95, 0);
                this.plane.rotation.x = -Math.PI / 2;
                this.plane.visible = false;
                this.currentScene.scene.add(this.plane);
            }

            events() {
                let that= this;
                //document.addEventListener('pointermove', function (event) {
                that.currentScene.renderer.domElement.addEventListener('pointermove', function (event) {
                    if(MODE === 'building') {
                        that.mousePos = event;
                        that.snap.mouseMove(event);
                    }
                }, false);
                that.currentScene.renderer.domElement.addEventListener('pointerdown', function(event) {
                    if(MODE === 'building') {
                        that.snap.mouseDown(event);
                    }
                }, true);
                that.currentScene.renderer.domElement.addEventListener('pointerup', function(event) {
                    if(MODE === 'building') {
                        that.snap.mouseUp(event);
                    } else if(MODE === 'painting') {
                        that.snap.paint(event);
                    }
                }, true);

                document.addEventListener('keydown', function(event) {
                    if(event.code === "ControlLeft") {
                        that.snap.addRemoveAction(1);
                        that.currentMarker[that.chosenMarker].traverse(function(object) {
                            if(object.isMesh) object.material.color = new THREE.Color(0xFFFF00);
                        });
                    }
                }, true);
                document.addEventListener('keyup', function(event) {
                    if(event.code === "ControlLeft") {
                        that.snap.addRemoveAction(0);
                        that.currentMarker[that.chosenMarker].traverse(function(object) {
                            if(object.isMesh) object.material.color = new THREE.Color(0x00FF00);
                        });
                    }
                }, true);

                window.addEventListener('changeT', function (e) {
                    that.snap.changeTemplate(e.detail.id);
                });

                window.addEventListener('changeModel', function (e) {
                    that.chosenMarker = e.detail.id;
                    that.snap.changeModel(that.currentMarker[e.detail.id], that.currentModel[e.detail.id]);
                });

                window.addEventListener('loadBricks', function (e) {
                    that.snap.loadBricks();
                });

                window.addEventListener('saveBricks', function (e) {
                    that.snap.generateBrickMap2();
                });

                window.addEventListener('switchPaint', function (e) {
                    that.snap.hideMarker();
                });
            }

            update() {
                if(this.mousePos !== null) {
                    //this.snap.mouseMove(this.mousePos);
                }
            }

            LoadModel() {
                let that        = this;
                let path        = "./models/";
                let modelFile   = "lego_brick.glb";
                let file        = path + modelFile;

                gltfLoaderDraco.load(file, function (gltf) {
                    that.model = gltf.scene;
                    that.model.scale.set(1.26, 1.26, 1.26);
                    /*that.model.traverse( function ( object ) {
                        if ( object.isMesh) {
                            object.material.onBeforeCompile = ModifyShader_;
                            object.receiveShadow = true;
                            object.castShadow = true;
                            object.material.side = THREE.FrontSide;
                        }
                    });

                    that.model.scale.set(1.26, 1.26, 1.26);
                    that.model.position.y = 8.95;
                    that.model.name = 'brick';
                    that.model.attr = {width:2,length:4,height:4};*/

                    that.instanceGeometry = new THREE.BufferGeometry();
                    that.model.traverse( function ( child ) {
                        if (child.isMesh) {
                            that.instanceGeometry.copy(child.geometry);
                        }
                    });
                    that.newModel = new THREE.Mesh(that.instanceGeometry,new THREE.MeshPhongMaterial({
                        //color:0xae0606,
                        color:new THREE.Color(BRICK_COLOUR),
                        reflectivity: 1.0,
                        shininess: 200.0,
                        emissive:0x000000,
                        emissiveIntensity:5.0,
                        specular:0x111111,
                        side:THREE.FrontSide
                    }));
                    that.newModel.material.onBeforeCompile = ModifyShader_;
                    that.newModel.scale.set(126, 126, 126);
                    that.newModel.position.y = 8.95;
                    that.newModel.rotation.x = -Math.PI / 2;
                    that.newModel.name = 'brick';
                    that.newModel.userData.attr = {width:2,length:4,height:4,type:0};
                    that.newModel.castShadow = true;
                    that.newModel.receiveShadow = true;

                    that.marker = that.model.clone();
                    that.marker.traverse( function ( object ) {
                        if ( object.isMesh) {
                            object.material = new THREE.MeshStandardMaterial({
                                color:0x00FF00,
                                opacity:0.3,
                                transparent: true,
                                side: THREE.DoubleSide
                            });
                            object.receiveShadow = false;
                            object.castShadow = false;
                        }
                    });
                    that.marker.name = 'marker';

                    that.currentMarker.push(that.marker);
                    that.currentModel.push(that.newModel);

                    that.loadGrid();
                    that.snap = new GridSnap(
                        that.currentScene.scene,
                        that.currentScene.renderer,
                        that.currentScene.camera,
                        that.plane,
                        that.snapRadius,
                        that.marker,
                        that.newModel
                    );

                    that.events();
                    that.loaded = true;

                    that.LoadModel2();

                    LOADED++;
                });
            }

            LoadModel2() {
                let that        = this;
                let path        = "./models/";
                let modelFile   = "lego_brick2x2.glb";
                let file        = path + modelFile;

                gltfLoaderDraco.load(file, function (gltf) {
                    that.model = gltf.scene;
                    that.model.scale.set(1.26, 1.26, 1.26);

                    that.instanceGeometry = new THREE.BufferGeometry();
                    that.model.traverse( function ( child ) {
                        if (child.isMesh) {
                            that.instanceGeometry.copy(child.geometry);
                        }
                    });
                    that.newModel = new THREE.Mesh(that.instanceGeometry,new THREE.MeshPhongMaterial({
                        color:0xae0606,
                        reflectivity: 1.0,
                        shininess: 200.0,
                        emissive:0x000000,
                        emissiveIntensity:5.0,
                        specular:0x111111,
                        side:THREE.FrontSide
                    }));
                    that.newModel.material.onBeforeCompile = ModifyShader_;
                    that.newModel.scale.set(1.26, 1.26, 1.26);
                    that.newModel.position.y = 8.95;
                    that.newModel.name = 'brick';
                    that.newModel.userData.attr = {width:2,length:2,height:4,type:1};
                    that.newModel.castShadow = true;
                    that.newModel.receiveShadow = true;

                    that.marker = that.model.clone();
                    that.marker.traverse( function ( object ) {
                        if ( object.isMesh) {
                            object.material = new THREE.MeshStandardMaterial({
                                color:0x00FF00,
                                opacity:0.3,
                                transparent: true,
                                side: THREE.DoubleSide
                            });
                            object.receiveShadow = false;
                            object.castShadow = false;
                        }
                    });
                    that.marker.name = 'marker';

                    that.currentMarker.push(that.marker);
                    that.currentModel.push(that.newModel);
                });
            }
        }
    }
})();