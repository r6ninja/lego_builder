/**
 * Player character
 */
import * as THREE from './3rdParty/three.module.js'

import {GLTFLoader} from './3rdParty/loaders/GLTFLoader.js'
import {DRACOLoader} from './3rdParty/loaders/DRACOLoader.js'

const gltfLoaderDraco = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath( 'js/3rdParty/loaders/draco/gltf/' );
dracoLoader.setDecoderConfig({type:'js'});
gltfLoaderDraco.setDRACOLoader( dracoLoader );

export const creatureFlying = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                var that = this;
                this.clock = new THREE.Clock();
                this.f = 0;
                this.isMoving = true;
                this.roar = false;
                this.M3 = new THREE.Matrix3();
                this.M4 = new THREE.Matrix4();
                this.n  = new THREE.Vector3(); // normal,
                this.n0 = new THREE.Vector3(0,1 ,0); // normal, first up
                this.b  = new THREE.Vector3(); // binormal
                this.p  = new THREE.Vector3();
                this.axisX = new THREE.Vector3(1,0,0);
                this.chooseFromAnimations = [0,2,4];
                this.randomElement = 0;

                this.LoadModel();
                this.LoadPath();
            }

            update(t) {

                let check = true;
                if(this.model && check) {
                    let delta = this.clock.getDelta();
                    if (this.mixer) this.mixer.update(delta);

                    if (this.isMoving) {
                        if (this.f === 0 || this.f > 1) {
                            this.n.copy(this.n0);
                            this.f = 0; // loop
                        }

                        this.f += delta / 50;

                        this.t = this.curve.getTangent(this.f);
                        this.b.crossVectors(this.t, this.n);
                        this.n.crossVectors(this.t, this.b.negate());

                        this.M3.set(-this.t.x, this.n.x, this.b.x,
                            -this.t.y, this.n.y, this.b.y,
                            -this.t.z, this.n.z, this.b.z);
                        this.M4 = this.setFromMatrix3(this.M4, this.M3);

                        this.p = this.curve.getPoint(this.f);

                        this.box.setRotationFromMatrix(this.M4);
                        this.box.position.set(this.p.x, this.p.y, this.p.z);
                    }
                }
            }

            setFromMatrix3(m4, m3) {
                this.me = m3.elements;
                m4.set(
                    this.me[ 0 ], this.me[ 3 ], this.me[ 6 ], 0,
                    this.me[ 1 ], this.me[ 4 ], this.me[ 7 ], 0,
                    this.me[ 2 ], this.me[ 5 ], this.me[ 8 ], 0,
                    0, 0, 0, 1
                );
                return m4;
            }

            LoadPath() {
                let yAxis = 150;
                const somePoints = [
                    new THREE.Vector3(300, yAxis,250),
                    new THREE.Vector3(275, yAxis,375),
                    new THREE.Vector3(0, yAxis,400),

                    new THREE.Vector3(0, yAxis,0),

                    new THREE.Vector3(300, yAxis,-300),
                    new THREE.Vector3(400, yAxis,-275),
                    new THREE.Vector3(300, yAxis,0),

                    new THREE.Vector3(0, yAxis,0),

                    new THREE.Vector3(-260, yAxis,-250),
                    new THREE.Vector3(-100, yAxis,-360),
                    new THREE.Vector3(50, yAxis,-320),

                    new THREE.Vector3(0, yAxis,0),

                    new THREE.Vector3(-50, yAxis,300),
                    new THREE.Vector3(-200, yAxis,300),
                    new THREE.Vector3(-200, yAxis,0),

                    new THREE.Vector3(0, yAxis,0),
                ];

                const p = new THREE.Points(new THREE.BufferGeometry().setFromPoints(somePoints), new THREE.PointsMaterial({color: "red", size: 10}));
                //this.currentScene.scene.add(p);

                this.curve = new THREE.CatmullRomCurve3(somePoints);
                this.curve.closed = true;

                const points = this.curve.getPoints(150);
                const line = new THREE.LineLoop( new THREE.BufferGeometry().setFromPoints(points), new THREE.LineBasicMaterial({color: 0xffffaa}));
                //this.currentScene.scene.add(line);
            }

            LoadModel() {
                let that        = this;
                let path        = "./models/creature/pterodactyl/";
                //let modelFile   = "scene.gltf";
                let modelFile   = "pterodactyl2.glb";
                let file        = path + modelFile;

                gltfLoaderDraco.load(file, function (gltf) {
                    that.model = gltf.scene;
                    that.mixer = new THREE.AnimationMixer( that.model );

                    that.model.traverse( function ( object ) {
                        if (object.isMesh) {
                            object.receiveShadow = true;
                            object.castShadow = true;
                            object.material.side = THREE.FrontSide;
                            object.material.onBeforeCompile = ModifyShader_;
                        }
                    });

                    that.model.position.set(0, 0, 0);
                    //that.model.scale.set(0.075, 0.075, 0.075);
                    that.model.scale.set(100, 100, 100);
                    that.model.rotation.y = -Math.PI / 2;
                    that.model.name = 'creature';

                    that.loaded = true;

                    that.box = new THREE.Mesh(
                        new THREE.BoxBufferGeometry(1, 1, 1, 1, 1, 1),
                        new THREE.MeshBasicMaterial({transparent:true, opacity:0.0})
                    );
                    that.box.add(that.model);
                    that.currentScene.scene.add(that.box);

                    that.animations = gltf.animations;

                    that.currentclip = gltf.animations[that.randomElement];
                    that.currentAction = that.mixer.clipAction(that.currentclip).play();

                    LOADED++;
                });
            }
        }
    }
})();