/**
 * Add a physics ball to the scene
 */
import * as THREE from './3rdParty/three.module.js'
export const ball = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                let that = this;

                const sphere_geometry = new THREE.SphereGeometry( 10, 32, 32 );
                const material = new Physijs.createMaterial(
                    new THREE.MeshPhongMaterial({ color: 0x00FF00 }),
                    0,
                    0.6
                );
                this.shape = new Physijs.SphereMesh(
                    sphere_geometry,
                    material,
                    10
                );
                this.shape.castShadow = true;
                this.shape.receiveShadow = true;
                this.shape.position.set(0, 75, 0);
                this.shape.layers.enable(BLOOM_SCENE);

                if(BETTER_FOG || BETTER_FOG2) {
                    this.shape.material.onBeforeCompile = ModifyShader_;
                }

                this.loadIntoPostProcessing();
                this.currentScene.scene.add(this.shape);

                LOADED++;
            }

            loadIntoPostProcessing() {
                let that = this;
                return function load() {
                    return (typeof that.currentScene.selectiveBloomEffect != 'undefined') ? that.currentScene.selectiveBloomEffect.selection.add(that.shape) : setTimeout(load, 250);
                }();
            }
        }
    }
})();