<?php $version = 1; ?>
<?php if(!isset($_REQUEST['quality']) || empty($_REQUEST['quality'])) { ?>
<!DOCTYPE HTML>
<html>
<head>
    <title>conceptninja.co.za - Lego Builder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicons/favicon-16x16.png">
    <link rel="manifest" href="images/favicons/site.webmanifest">
    <link rel="mask-icon" href="images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/choose.css?v=<?php echo $version; ?>" />
</head>
<body>
<div id="blocker">
    <div id="instructions" class="instructions">
        <div>
            <a href="?quality=low">
                <span>Low Quality</span>
                <sub>(no fog, no post-processing, no creatures)</sub>
                <img src="images/low-quality.png" alt="" title="Low Quality" />
            </a>
        </div>
        <div>
            <a href="?quality=medium">
                <span>Medium Quality</span>
                <sub>(no fog, no post-processing, includes creatures)</sub>
                <img src="images/medium-quality.png" alt="" title="Medium Quality" />
            </a>
        </div>
        <div>
            <a href="?quality=high">
                <span>High Quality</span>
                <sub>(fog, post-processing and creatures)</sub>
                <img src="images/high-quality.png" alt="" title="High Quality" />
            </a>
        </div>
    </div>
</div>
</body>
</html>
<?php } else { ?>
<!DOCTYPE HTML>
<html>
<head>
    <title>conceptninja.co.za - Lego Builder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicons/favicon-16x16.png">
    <link rel="manifest" href="images/favicons/site.webmanifest">
    <link rel="mask-icon" href="images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/main.css?v=<?php echo $version; ?>" />
    <script src="js/3rdParty/lazy.min.js?v=<?php echo $version; ?>" async=""></script>
</head>
<body>
<div class="body" id="blah">
    <canvas id="threejs"></canvas>
    <div class="templates">
        <ul>
            <li class="gloss sTemp" data-id="0"><span>Free<br />Build</span><img src="images/templates/template0_small2.jpg" data-id="0" alt="" title="Free build"></li>
            <li>&nbsp;</li>
            <li class="gloss sTemp" data-id="1"><img src="images/templates/template1_small.jpg" data-id="1" alt="" title="Shadow 1"></li>
            <li>&nbsp;</li>
            <li class="gloss sTemp" data-id="2"><img src="images/templates/template2_small.jpg" data-id="2" alt="" title="Shadow 2"></li>
            <li>&nbsp;</li>
            <li><a class="loadBricksButton buttons">Load</a></li>
            <li>&nbsp;</li>
            <li><a class="copyBricksButton buttons">Save</a></li>
        </ul>
    </div>
    <div class="tools">
        <ul>
            <li class="gloss mode" id="buildBricks"><span>Build</span><img src="images/build.png" alt="" title="Build"></li>
            <li>&nbsp;</li>
            <li class="" id="colourPick"><span>Paint</span><img src="images/colour.jpg" alt="" title="Colour picker"></li>
        </ul>
    </div>
    <div class="models">
        <ul>
            <li class="gloss bricks" data-id="0"><img src="images/2x4.jpg" data-id="0" alt="" title="Brick 1"></li>
            <li>&nbsp;</li>
            <li class="gloss bricks" data-id="1"><img src="images/2x2.jpg" data-id="1" alt="" title="Brick 2"></li>
        </ul>
    </div>
    <div id='soundBars' class="" style="display: none;">
            <!--<div class="play-button"></div>-->
        <a class="play-btn" href="#"></a>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
    </div>
    <div class="section fp-auto-height" data-section-name="about" id="eventsHere">
        <div class="webglHolderSingle" id="test">
            <div class="webglInnerHolder">
                <div class="container0" id="container0"></div>
            </div>
        </div>
    </div>
    <div>
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div id="loader_text">Loading...</div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
    </div>
</div>
<div id="blocker">
    <div id="instructions">
        <span style="font-size:24px">Click to start</span><br/>&nbsp;<br />
        Fill the outline on the screen with bricks, no more, no less.<br />&nbsp;<br />
        Choose a new shape on the right panel, or select 'free build' for fun.<br />&nbsp;<br />
        To Build: Click on the 'Build' icon<br/>
        To Paint: Click on the 'Paint' icon<br/>
        Place Brick: MOUSE Click<br/>
        Remove Brick: CTRL + MOUSE Click<br/>
        Zoom: MOUSE Wheel<br/>
        Rotate: Left MOUSE + Drag<br/>
        Pan: Right MOUSE + Drag
    </div>
</div>
<div class="objectForm loadBrickString">
    <div class="objectFormWrapper">
        <a class="closeBtn">X</a>
        <form name="brickLoader" id="brickLoader" action="" method="post">
            <label for="loadBrickCoOrds">Paste brick string here:</label>
            <textarea name="brickCoOrds" id="loadBrickCoOrds" rows="5"></textarea>
            <button type="submit" class="buttons" id="loadString">Load Bricks</button>
        </form>
    </div>
</div>
<div class="objectForm copyBrickString">
    <div class="objectFormWrapper">
        <a class="closeBtn">X</a>
        <form name="brickLoader" id="brickLoader" action="" method="post">
            <label for="copyBrickCoOrds">Copy brick string below:</label>
            <textarea name="brickCoOrds" id="copyBrickCoOrds" rows="5" readonly></textarea>
            <button type="submit" class="buttons" id="copyToClipboard">Copy String</button>
        </form>
    </div>
</div>
<script>
    var QUALITY = <?php echo (@$_REQUEST['quality'] === 'low' || @$_REQUEST['quality'] === 'medium') ? 'false' : 'true'; ?>;
    var QUALITY_LEVEL = '<?php echo @$_REQUEST['quality']; ?>';
</script>
<!-- jQuery -->
<script src="js/3rdParty/jquery-3.5.1.min.js?v=<?php echo $version; ?>"></script>
<script src="js/3rdParty/jquery.scrollify.js?v=<?php echo $version; ?>"></script>
<script src="js/3rdParty/vanilla-picker.js?v=<?php echo $version; ?>"></script>

<!-- Main program script -->
<script src="js/common/vars.js?v=<?php echo $version; ?>"></script>

<script type="module" src="js/main.js?v=<?php echo $version; ?>"></script>
<!--<script type="module" src="js/bundle.min.js?v=<?php /*echo $version; */?>"></script>-->
<script type="module" src="js/extra.js?v=<?php echo $version; ?>"></script>
</body>
</html>
<?php } ?>